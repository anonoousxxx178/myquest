from django.db import models
from django.contrib.auth import get_user_model


class GameScenario(models.Model):
    name = models.CharField(max_length=32)

    def to_dict(self):
        return {"name": self.name}


class Level(models.Model):
    game_scenario = models.ForeignKey(GameScenario, on_delete=models.CASCADE)
    order_number = models.IntegerField()
    name = models.CharField(max_length=32)
    clue1 = models.CharField(max_length=32)
    clue2 = models.CharField(max_length=32)
    code = models.CharField(max_length=64)
    description = models.CharField(max_length=512)

    def to_dict(self):
        return {"name": self.name, "order_number": self.order_number, "description": self.description}


class Game(models.Model):
    name = models.CharField(max_length=32)
    game_scenario = models.ForeignKey(GameScenario, on_delete=models.CASCADE, default=None)
    description = models.CharField(max_length=512)
    photo = models.CharField(max_length=64)
    users = models.ManyToManyField(get_user_model())

    def to_dict(self):
        return {"id": self.pk, "name": self.name, "description": self.description, "photo": self.photo}


class UserGameState(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    current_clue = models.IntegerField()
    current_level = models.ForeignKey(Level, on_delete=models.CASCADE, default=None)
    status = models.CharField(max_length=32)

    def to_dict(self):
        return {"user": self.user, "game": self.game, "current_clue": self.current_clue,
                "current_level": self.current_level, "status": self.status}


class UserLevelState(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    start_time = models.IntegerField(default=0)
    end_time = models.IntegerField(default=0)
    penalty = models.IntegerField(default=0)

    def to_dict(self):
        return {"user": self.user.username, "level": self.level.name,
                "start_time": self.start_time, "end_time": self.end_time, "penalty": self.penalty}
