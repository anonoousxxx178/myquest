from datetime import datetime
import calendar


def get_current_unix_time():
    return calendar.timegm(datetime.utcnow().utctimetuple())
