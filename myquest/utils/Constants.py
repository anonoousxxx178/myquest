RUNNING = "running"
FINISHED = "finished"
NOT_STARTED = "not started"
# 5 minutes
CLUE_PENALTY = 60 * 5
