from django.contrib.auth import authenticate, get_user_model
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
from myquest.models import Game, UserGameState, Level, UserLevelState
from django.http import JsonResponse
from .utils import Constants, Utils


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'}, status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'}, status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key}, status=HTTP_200_OK)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def register(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'}, status=HTTP_400_BAD_REQUEST)
    user = get_user_model().objects.filter(username=username).first()
    if user:
        return Response({'error': 'Username is taken already'}, status=HTTP_400_BAD_REQUEST)
    user = get_user_model().objects.create(username=username)
    user.set_password(password)
    user.save()
    return Response(status=HTTP_201_CREATED)


@csrf_exempt
@api_view(["GET"])
def games(request):
    games = Game.objects.all()
    result = []
    for game in games:
        joined = check_user_joined_game(request.user, game)
        game = game.to_dict()
        game["joined"] = joined
        result.append(game)
    return JsonResponse({"games": result})


@csrf_exempt
@api_view(["POST"])
def join_game(request):
    game_id = request.data.get("game_id")
    game = Game.objects.get(pk=game_id)
    if check_user_joined_game(request.user, game):
        return Response({'error': 'Joined the game already!'}, status=HTTP_400_BAD_REQUEST)
    game.users.add(request.user)
    game.save()
    return Response(status=HTTP_200_OK)


@csrf_exempt
@api_view(["POST"])
def start_game(request):
    now = Utils.get_current_unix_time()
    game_id = request.data.get("game_id")
    game = Game.objects.get(pk=game_id)
    if not check_user_joined_game(request.user, game):
        return Response({'error': 'Join the game first!'}, status=HTTP_400_BAD_REQUEST)
    user_game_state = UserGameState.objects.filter(game_id=game_id, user_id=request.user.id).first()
    # game started already
    if user_game_state is not None:
        return Response({'error': 'Game started already!'}, status=HTTP_400_BAD_REQUEST)
    game_scenario = game.game_scenario_id
    levels = Level.objects.filter(game_scenario=game_scenario)

    # get first level by order number
    # pre-populate the level stats
    start_level = None
    level_stats = []
    for level in levels:
        level_stat = UserLevelState(user=request.user, level=level, game=game, start_time=0, end_time=0, penalty=0)
        if level.order_number == 1:
            level_stat.start_time = now
            start_level = level
        level_stats.append(level_stat)
    if start_level is None:
        return Response({'error': 'Game error. Contact admins'}, status=HTTP_400_BAD_REQUEST)
    UserLevelState.objects.bulk_create(level_stats)

    user_game_state = UserGameState.objects.create(user_id=request.user.id, game_id=game_id, current_clue=0,
                                                   current_level=start_level, status=Constants.RUNNING)
    user_game_state.save()
    return Response(status=HTTP_200_OK)


@csrf_exempt
@api_view(["GET"])
def current_state(request):
    game_id = request.data.get("game_id")
    game = Game.objects.get(pk=game_id)
    if not check_user_joined_game(request.user, game):
        return Response({'error': 'Join the game first!'}, status=HTTP_400_BAD_REQUEST)
    user_game_state = UserGameState.objects.filter(game_id=game_id, user_id=request.user.id).first()
    # game was not started yet
    if user_game_state is None:
        return JsonResponse({"status": Constants.NOT_STARTED})
    level = user_game_state.current_level

    # get all collected clues
    clues = []
    if user_game_state.current_clue >= 1:
        clues.append(level.clue1)
    if user_game_state.current_clue >= 2:
        clues.append(level.clue2)
    level_dict = level.to_dict()
    level_dict["clues"] = clues

    result = {"status": user_game_state.status}
    # if game was finished just return status
    if user_game_state.status != Constants.FINISHED:
        result["level"] = level_dict
    return JsonResponse(result)


@csrf_exempt
@api_view(["POST"])
def take_clue(request):
    game_id = request.data.get("game_id")
    user_game_state = UserGameState.objects.filter(game_id=game_id, user_id=request.user.id).first()
    if user_game_state is None:
        return Response({'error': 'Game not started yet!'}, status=HTTP_400_BAD_REQUEST)
    if user_game_state.status == Constants.FINISHED:
        return Response({'error': 'Game finished already!'}, status=HTTP_400_BAD_REQUEST)
    if user_game_state.current_clue == 2:
        return Response({'error': 'You have taken all clues already!'}, status=HTTP_400_BAD_REQUEST)
    user_game_state.current_clue += 1
    user_game_state.save()

    # update stat penalty
    user_level_state = UserLevelState.objects.get(user_id=request.user.id, game_id=game_id,
                                                  level_id=user_game_state.current_level.id)
    user_level_state.penalty += Constants.CLUE_PENALTY
    user_level_state.save()

    return Response(status=HTTP_200_OK)


@csrf_exempt
@api_view(["POST"])
def code(request):
    now = Utils.get_current_unix_time()
    game_id = request.data.get("game_id")
    code = request.data.get("code")
    user_game_state = UserGameState.objects.filter(game_id=game_id, user_id=request.user.id).first()
    if user_game_state is None:
        return Response({'error': 'Game not started yet!'}, status=HTTP_400_BAD_REQUEST)
    if user_game_state.status == Constants.FINISHED:
        return Response({'error': 'Game finished already!'}, status=HTTP_400_BAD_REQUEST)

    # check if code is right(lower case)
    if user_game_state.current_level.code.lower() != code.lower():
        return Response({'error': 'Wrong code'}, status=HTTP_400_BAD_REQUEST)

    # set old level state start time
    old_level_state = UserLevelState.objects.get(user_id=request.user.id, game_id=game_id,
                                                 level_id=user_game_state.current_level.id)
    old_level_state.end_time = now
    old_level_state.save()

    # find new level
    new_level_number = user_game_state.current_level.order_number + 1
    new_level = None
    levels = Level.objects.filter(game_scenario=Game.objects.get(pk=game_id).game_scenario)
    # get first level by order number
    for level in levels:
        if level.order_number == new_level_number:
            new_level = level
            break
    # if we did not find the next level mark game as finished
    if new_level is None:
        user_game_state.status = Constants.FINISHED
    else:
        # set new level state start time
        new_level_state = UserLevelState.objects.get(user_id=request.user.id, game_id=game_id,
                                                     level_id=new_level.id)
        new_level_state.start_time = now
        new_level_state.save()
        # update the new level and clear the clues counter
        user_game_state.current_level = new_level
        user_game_state.current_clue = 0

    user_game_state.save()

    return Response(status=HTTP_200_OK)


@csrf_exempt
@api_view(["GET"])
def stats(request):
    game_id = request.data.get("game_id")
    result = []
    for stat in UserLevelState.objects.filter(game_id=game_id):
        result.append(stat.to_dict())

    return Response({"stats": result}, status=HTTP_200_OK)


def check_user_joined_game(user, game):
    joined = False
    for game_user in list(game.users.all()):
        if game_user.username == user.username:
            joined = True
            break
    return joined
