from django.contrib import admin
from .models import Game, GameScenario, Level, UserGameState, UserLevelState

admin.site.register(Game)
admin.site.register(GameScenario)
admin.site.register(Level)
admin.site.register(UserGameState)
admin.site.register(UserLevelState)
