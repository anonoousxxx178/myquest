"""myquest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import login, register, games, join_game, start_game, current_state, take_clue, code, stats

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login', login),
    path('api/register', register),
    path('api/games', games),
    path('api/join_game', join_game),
    path('api/start_game', start_game),
    path('api/current_state', current_state),
    path('api/take_clue', take_clue),
    path('api/code', code),
    path('api/stats', stats)
]
